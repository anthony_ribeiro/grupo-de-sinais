<?php
require_once("header.html");
?>

	<div class="container">
		
		<table class="table table-striped table-bordered">
			
			<thead>
				<tr>
					<th>Surdo</th>
					<th>Data da visita</th>
					<th>Publicador</th>
					<th>Resultado</th>
				</tr>
			</thead>

			<tbody>
				<tr>
					<td></td>
					<td>__/__/____</td>
					<td></td>
					<td>
						<input type="checkbox"> Encontrado <br>
						<input type="checkbox"> Não Encontrado <br>
						<input type="checkbox"> Não quer ser visitado <br>
					</td>
				</tr>

				<tr>
					<td></td>
					<td>__/__/____</td>
					<td></td>
					<td>
						<input type="checkbox"> Encontrado <br>
						<input type="checkbox"> Não Encontrado <br>
						<input type="checkbox"> Não quer ser visitado <br>
					</td>
				</tr>

				<tr>
					<td></td>
					<td>__/__/____</td>
					<td></td>
					<td>
						<input type="checkbox"> Encontrado <br>
						<input type="checkbox"> Não Encontrado <br>
						<input type="checkbox"> Não quer ser visitado <br>
					</td>
				</tr>
			</tbody>

		</table>

	</div>

<?php
require_once("footer.html");
?>